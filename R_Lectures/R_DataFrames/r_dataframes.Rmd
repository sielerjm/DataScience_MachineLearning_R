---
title: "R DataFrames"
author: "Michael Sieler"
date: "12/29/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Data Frame Basics

```{r}

## Built in data frames found in R
#state.x77
#USPersonalExpenditure
#women
# data() # Collection of all of R's built in data frames

#str(state.x77)
#summary(state.x77)

days <- c("mon", "tue", "wed", "thurs", "fri")
temp <- c(22.1, 22.3, 23.1, 25.7, 21.9)
rain <- c(T, T, T, F, T)

df <- data.frame(days, temp, rain)
df

```



## Data Frame Selection and Indexing

```{r}

#dataframe[row, col]

# df[1:5,c("days", "temp")]  # select a subset of days, all rows but days and temp columns

df$days  # returns a vector
df['days']  # returns a dataframe

subset(df, subset = rain == TRUE)  # returns which days if rain is true
subset(df, subset = temp > 23)  # returns days that have temp > 23

sorted.temp <- order(df["temp"])
sorted.temp
df[sorted.temp,]
desc.temp <- order(-df['temp'])  # orders by descending numbers (big to small)
df[desc.temp,] 

```



## Overview of Data Frames (!Important!)

### Creating Data Frames

```{r}

empty <- data.frame()  # makes an empty dataframe
c1 <- 1:10
c2 <- letters[1:10]
c1
c2

df <- data.frame(col.name.1 = c1, col.name.2 = c2)
df


```

### Importing and Exporting Data

```{r}

d2 <- read.csv('example.csv')
write.csv(df, file = "saved_df.csv")
read.csv("saved_df.csv")

```

### Getting Information about Data Frames

```{r}

nrow(df)
ncol(df)

colnames(df)
rownames(df)
str(df)
summary(df)


```

### Referencing Cells

```{r}

# referencing index locations in a dataframe: dataframe[[row, col]]

df[[5,2]]

df[[5, "col.name.2"]]  # use the name of the column to call it instead of the index value

df[[2, "col.name.2"]] <- 9999  # change value at a specific location
df

# referencing rows in a dataframe: dataframe[row,]

df[1,]

# referencing cols in a dataframe (returns vectors of values)

head(mtcars)
mtcars$mpg
mtcars[,"mpg"]
mtcars[,1]
mtcars[["mpg"]]

mtcars["mpg"]  # returns a dataframe
mtcars[1] # returns a dataframe


head(mtcars[c("mpg", "cyl")])  # return multiple columns




```

### Referencing Rows

```{r}

df2 <- data.frame(col.name.1 = 2000, col.name.2 = "new")
df2

dfnew <- rbind(df, df2)
dfnew




```

### Referencing Columns

```{r}


```

### Adding Rows

```{r}

df$newcol <- 2*df$col.name.1  # makes a copy of col.names.1 
df$newcol.copy <- df$newcol  # makes a copy of col.names.1
df[,'newcol.copy2'] <- df$newcol  # Alternate way of copying and creating a new column
df


```

### Adding Columns

```{r}


```

### Setting Column Names

```{r}

colnames(df)
colnames(df) <- c("1", "2", "3", "4", "5")
df

colnames(df)[1] <- "NEW COL NAME"
df



```

### Selecting Multiple Rows

```{r}

df[1:10,]
df[1:3,]
head(df,7)
df[-2,]  # Select everything but row 2

mtcars[mtcars$mpg > 20,]  # Selecting rows where mpg > 20 and all columns.
mtcars[(mtcars$mpg > 20) & (mtcars$cyl == 6),]  # filter for two conditions

mtcars[(mtcars$mpg > 20) & (mtcars$cyl == 6), c("mpg", "cyl", "hp")]  # selects for two conditions and three specific columns


subset(mtcars, mpg > 20 & cyl == 6)  # a simpler way to subset


```

### Selecting Multiple Columns

```{r}

head(mtcars)
mtcars[, c(1, 2, 3, 4)]
mtcars[,c("mpg", "cyl")]

```

### Dealing with Missing Data

```{r}

# Any missing datapoints (eg "na")
is.na(mtcars)  # returns true or false in the entire data frame if there is "NA"
any(is.na(df))  # returns true or false if any one cell is missing data
any(is.na(mtcars$mpg)) # see if missing data in a specific column

df[is.na(df)] <- 0  # replaces any "NA"'s with "0"



```



## Data Frames Exercise

```{r}

Names = c("Sam", "Frank", "Amy")
Age = c(22, 25, 26)
Weight = c(150, 165, 120)
Sex = c("M", "M", "F")

df <- data.frame(Names, Age, Weight, Sex)
df
is.data.frame(df)

colnames(df)[1] <- ""
df



is.data.frame(mtcars)
mat <- matrix(1:25, nrow = 5)
mat

df.mat <- as.data.frame(mat)
df.mat



df <- mtcars

df[1:6,]

mean(df$mpg)


df[df$cyl == 6,]
df[,c('am', 'gear', 'carb')]


performance <- df$hp / df$wt
head(performance)

df[,"performance"] <- round(performance, digits = 2)

head(df)

mtcars[(mtcars$mpg > 20) & (mtcars$cyl == 6),] 

df2 <- df[(df$hp > 100) & (df$wt > 2.5),]

mean(df2$mpg)

df["Hornet Sportabout", "mpg"]

```


